convertExcel = require("excel-as-json").processFile;
const messages = require("./messages.json");
const fs = require("fs");

convertExcel("source.xlsx", null, { isColOriented: true }, (err, data) => {
  if (err) return console.log("convertExcel err", err);

  syncJsonData(data);
});

function isEmpty(string) {
  return (
    typeof string == "undefined" ||
    string == null ||
    string == false ||
    string.length == 0 ||
    string == "" ||
    string == " " ||
    string.replace(/\s/g, "") == "" ||
    !/[^\s]/.test(string) ||
    /^\s*$/.test(string)
  );
}

function syncJsonData(jsonData) {
  let outputJson = {};

  // set existing data
  for (let property in messages) {
    const value = messages[property];
    outputJson[property] = value;
  }

  // set new translation data
  for (let property in jsonData[0]) {
    const value = jsonData[0][property];
    if (property == "Translation Key") continue;
    if (isEmpty(value)) continue;

    outputJson[property] = value.trim();
  }

  saveJsonFile(outputJson, "output.json");
}

function saveJsonFile(_jsonContent, filename) {
  const jsonContent = JSON.stringify(_jsonContent);

  fs.writeFile(filename, jsonContent, "utf8", err => {
    if (err) {
      return console.log(
        "An error occured while writing JSON Object to File.",
        err
      );
    }

    console.log("JSON file has been saved.");
  });
}
